package com.example.tpapi;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class PersonnageRepositoryImpl implements PersonnageRepository {

    private RedisTemplate<String, Personnage> redisTemplate;

    private HashOperations hashOperations;


    public PersonnageRepositoryImpl(RedisTemplate<String, Personnage> redisTemplate) {
        this.redisTemplate = redisTemplate;

        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void save(Personnage personnage) {
        hashOperations.put("PERSONNAGE", personnage.getId(), personnage);
    }

    @Override
    public Map<Long,Personnage> findAll() {
        return hashOperations.entries("PERSONNAGE");
    }

    @Override
    public Personnage findById(long id) {
        return (Personnage) hashOperations.get("PERSONNAGE", id);
    }

    @Override
    public void update(Personnage personnage) {
        save(personnage);
    }

    @Override
    public void delete(long id) {
        hashOperations.delete("PERSONNAGE", id);
    }
}
