package com.example.tpapi;

import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface PersonnageRepository  {
    void save(Personnage personnage);
    Map<Long,Personnage> findAll();
    Personnage findById(long id);
    void update(Personnage personnage);
    void delete(long id);

}