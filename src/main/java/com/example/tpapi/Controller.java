package com.example.tpapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/rest/perso")
public class Controller {

    @Autowired
    private PersonnageRepository personnageRepository;

    BeanConfig beanConfig = new BeanConfig();

    public Controller(PersonnageRepository personnageRepository) {
        this.personnageRepository = personnageRepository;
    }

    /**
     * ajoute un personnage et l'affiche
     * @param nom
     * @param prenom
     * @return
     */
    @GetMapping("/add/{id}/{nom}/{prenom}")
    public Personnage add(@PathVariable("id") final String id,@PathVariable("nom") final String nom,
                          @PathVariable("prenom") final String prenom) {
            Long idLong = Long.parseLong(id);
            Personnage personnage =new Personnage(idLong,nom, prenom);
            personnageRepository.save(personnage);
        return personnageRepository.findById(personnage.getId());
    }

    /**
     * Mise a jour d'un personnage
     * @param id
     * @param nom
     * @param prenom
     * @return
     */
    @GetMapping("/up/{id}/{nom}/{prenom}")
    public Personnage update(@PathVariable("id") final long id,@PathVariable("nom") final String nom,
                          @PathVariable("prenom") final String prenom) {
        Personnage personnage =new Personnage(nom, prenom);
        personnageRepository.update(personnage);
        return personnageRepository.findById(id);
    }

    /**
     * supprimer un personnage avec son id
     * @param id
     * @return
     */
    @GetMapping("/delete/{id}")
    public Map<Long, Personnage> delete(@PathVariable("id") final long id) {
        personnageRepository.delete(id);
        return all();
    }

    /**
     * Cette fonction retourne une map de tous les personnage
     * @return
     */
    @GetMapping("/all")
    public Map<Long, Personnage> all() {
        return personnageRepository.findAll();
    }


}