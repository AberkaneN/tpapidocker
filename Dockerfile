FROM java:8-jdk-alpine
LABEL VENDOR="Nazi aberkane"
LABEL com.example.tpapi.version="0.0.1-SNAPSHOT"
COPY target/tpapi-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.redis.uri = redis://redis:6379","-Djava.security.egd=file:/dev/./urandom", "-jar", "tpapi-0.0.1-SNAPSHOT.jar"]